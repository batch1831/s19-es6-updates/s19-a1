// console.log("Hello World")

let cube = (x) => x ** 3;

let total = cube(2);
console.log(`The cube of 2 is ${total}`);

let address = ["258", "Washington Ave.", "NW", "California", "90011"];
console.log(`I live at ${address[0]} ${address[1]} ${address[2]}, ${address[3]} ${address[4]}`);

const animal = {
	animalType: "saltwater crocodile", 
	animalWeight: "1075 kgs",
	animalLength: "20 ft 3 in"
};

const {animalType, animalWeight, animalLength} = animal;
console.log(`Lolong was a ${animalType}. He weighed at ${animalWeight} with a measurement of ${animalLength}.`);

let numbers = [1, 2 ,3 ,4 ,5];
numbers.forEach((number) => {
	console.log(number);
})

let reduceNumber = numbers.reduce((x,y) => x + y);

console.log(reduceNumber);

class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const classDog = new Dog();

classDog.name = "Frankie";
classDog.age = "5";
classDog.breed = "Miniature Daschund";

console.log(classDog)